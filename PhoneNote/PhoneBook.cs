﻿using System;
using System.Collections.Generic;

namespace Note
{
    class PhoneBook
    {
        private List<Contact> book = new List<Contact>();



        public Contact GetContactAt(int num)
        {
            return book[num];
        }

        public int ChooseContact()
        {
            Console.WriteLine("Введите номер контакта и нажмите Enter, что бы вернуться назад введите '0'.\n");
            ShortViewContacts();
            string textChoise = Console.ReadLine();
            int contactNumber;
            while (!int.TryParse(textChoise, out contactNumber) || contactNumber < 0 || contactNumber > book.Count)
            {
                Console.WriteLine("Введен некорректный номер контакта. Попробуйте еще раз.");
                textChoise = Console.ReadLine();
            }

            return contactNumber - 1;

        }

        public void AddContact(Contact contact)
        {
            book.Add(contact);
        }
        public void EditContact(Contact contact, int number)
        {
            book[number] = contact;
        }

        public void DeleteContact(int number)
        {
            book.RemoveAt(number);
        }

        public void ViewContacts()
        {
            foreach (Contact contact in book)
            {
                Console.WriteLine(contact.ToString(true));
            }
        }

        public void ShortViewContacts()
        {
            const int firstColumnWidth = 3;
            const int secondColumnWidth = 20;
            const int thirdColumnWidth = 20;
            const int fourthColumnWidth = 16;

            int counter = 1;

            Console.WriteLine($"{"№",firstColumnWidth}|{"Имя",-secondColumnWidth}|{"Фамилия",-thirdColumnWidth}|{"Номер телефона",-fourthColumnWidth}");
            foreach (Contact contact in book)
            {
                if (contact.Name.Length > secondColumnWidth - 3 && contact.Surname.Length > thirdColumnWidth - 3)
                {
                    Console.WriteLine($"{counter,firstColumnWidth}|{contact.Name.Substring(0, secondColumnWidth - 3) + "...",-secondColumnWidth}|{contact.Surname.Substring(0, thirdColumnWidth - 3) + "...",-thirdColumnWidth}|{contact.Phone,-fourthColumnWidth}");
                }
                else if (contact.Name.Length > secondColumnWidth - 3)
                {
                    Console.WriteLine($"{counter,firstColumnWidth}|{contact.Name.Substring(0, secondColumnWidth - 3) + "...",-secondColumnWidth}|{contact.Surname,-thirdColumnWidth}|{contact.Phone,-fourthColumnWidth}");
                }
                else if (contact.Surname.Length > thirdColumnWidth - 3)
                {
                    Console.WriteLine($"{counter,firstColumnWidth}|{contact.Name,-secondColumnWidth}|{contact.Surname.Substring(0, thirdColumnWidth - 3) + "...",-thirdColumnWidth}|{contact.Phone,-fourthColumnWidth}");
                }
                else
                {
                    Console.WriteLine($"{counter,firstColumnWidth}|{contact.Name,-secondColumnWidth}|{contact.Surname,-thirdColumnWidth}|{contact.Phone,-fourthColumnWidth}");
                }
                counter++;
            }
        }

        ~PhoneBook()
        {
            book.Clear();
        }

    }
}
