﻿using System;

namespace Note
{
    class Contact
    {
        private const int headingColumnWidth = 18;
        private const int valueColumnWidth = 100;
        private string name;
        private string surname;
        private string country;
        private string phone;

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Поле \"Имя\" не может быть пустой стркой");
                }
                this.name = value;
            }
        }
        public string Surname
        {
            get
            {
                return this.surname;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Поле \"Фамилия\" не может быть пустой стркой");
                }
                this.surname = value;
            }
        }
        public string Phone
        {
            get
            {
                return this.phone;
            }
            set
            {

                long temp;
                if (!long.TryParse(value, out temp))
                {
                    throw new ArgumentException("Номер телефона должен состоять из цифр");
                }
                if (temp < 0)
                {
                    throw new ArgumentException("Номер телефона не может быть отрицательным");
                }
                if(value.Trim('+').Length > 15)
                {
                    throw new ArgumentException("Согласно рекомендации ITU-T под номером E.164, номера могут иметь максимум 15 цифр.");
                }
                this.phone = "+" + value.Trim('+');
            }
        }
        public string Country
        {
            get
            {
                return this.country;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("Поле \"Страна\" не может быть пустой стркой");
                }
                this.country = value;
            }
        }
        public string Pathronymic { get; set; }
        public DateTime Dob { get; set; }
        public string Organization { get; set; }
        public string Post { get; set; }
        public string Other { get; set; }

        public string ToString(bool hideEmptyString = false)
        {
            return string.Concat(new string[]
            {
                TableFormat(this.Surname, "Фамилия", hideEmptyString),
                TableFormat(this.Name, "Имя", hideEmptyString),
                TableFormat(this.Pathronymic, "Отчество", hideEmptyString),
                TableFormat(this.Phone, "Номер телефона", hideEmptyString),
                TableFormat(this.Country, "Страна", hideEmptyString),
                TableFormat(this.Dob.ToString("dd.MM.yyyy"), "Дата рождения", hideEmptyString),
                TableFormat(this.Organization, "Организация", hideEmptyString),
                TableFormat(this.Post, "Должность", hideEmptyString),
                TableFormat(this.Other, "Заметки", hideEmptyString)
            });
        }
        private string TableFormat(string inputString, string heading, bool hideEmptyString)
        {
            if (hideEmptyString && (string.IsNullOrEmpty(inputString) || inputString == "01.01.0001"))
            {
                return "";
            }
            if(inputString == "01.01.0001")
            {
                inputString = "";
            }
            string text;
            if (inputString.Length > valueColumnWidth)
            {
                text = $"{heading,-headingColumnWidth}|{inputString.Substring(0, valueColumnWidth),-valueColumnWidth}\n";
                for (int i = 1; i < inputString.Length / valueColumnWidth + 1; i++)
                {
                    if (i == inputString.Length / valueColumnWidth)
                    {
                        text += $"{"",-headingColumnWidth}|{inputString.Substring(i * valueColumnWidth, inputString.Length % valueColumnWidth),-valueColumnWidth}\n";
                    }
                    else
                    {
                        text += $"{"",-headingColumnWidth}|{inputString.Substring(i * valueColumnWidth, valueColumnWidth),-valueColumnWidth}\n";
                    }
                }
            }
            else
            {
                text = $"{heading,-headingColumnWidth}|{inputString}\n";
            }
            return text;
        }




    }
}

