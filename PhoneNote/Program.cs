﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Note
{
    class Program
    {

        static PhoneBook phoneBook = new PhoneBook();
        private static void Main(string[] args)
        {

            while (true)
            {
                Console.WriteLine("Телефонная книга\n");

                Console.WriteLine("Добро пожаловать в консольное приложение Телефонная книга!\n");

                Console.WriteLine("Для создания нового контакта нажмите клавищу '1'");
                Console.WriteLine("Для редактирования созданого контакта нажмите '2'");
                Console.WriteLine("Для удаления созданного контакта нажмите '3'");
                Console.WriteLine("Для просмотра контактов нажмите '4'");
                Console.WriteLine("Для просмотра контактов в кратком ввиде нажмите '5'");
                Console.WriteLine("Для выхода из приложения нажмите '0'");

                switch (Console.ReadKey().KeyChar)
                {
                    case '0':
                        {
                            return;
                        }
                    case '1':
                        {
                            Console.Clear();
                            Console.WriteLine("Создание нового контакта\n");
                            Contact contact = new Contact();
                            ReadSurname(contact);
                            ReadName(contact);
                            ReadCountry(contact);
                            ReadPhone(contact);
                            ReadPathronymic(contact);
                            ReadDob(contact);
                            ReadOrganization(contact);
                            ReadPost(contact);
                            ReadOther(contact);
                            phoneBook.AddContact(contact);
                            Console.Clear();
                            Console.WriteLine("Контакт создан!");
                            Console.WriteLine("Для выхода в главное меню нажмите любую клавишу.");
                            Console.ReadKey();
                            break;
                        }
                    case '2':
                        {
                            Console.Clear();
                            Console.WriteLine("Редактирование контакта\n");
                            int number = phoneBook.ChooseContact();
                            if (number == -1)
                            {
                                break;
                            }
                            Contact contact = phoneBook.GetContactAt(number);

                            Console.Clear();
                            Console.WriteLine("Изменение контакта\n");
                            Console.WriteLine("Введите номер поля для его редактирования, что бы вернуться назад введите '0'\n");

                            string[] contactToView = contact.ToString().Split('\n');

                            for (int i = 0; i < contactToView.Length - 1; i++)
                            {
                                Console.WriteLine($"{i + 1}|{contactToView[i]}");
                            }

                            int numEdit = ChooseNumber();
                            while (numEdit > contactToView.Length - 1 && numEdit != 0)
                            {
                                Console.WriteLine("Введен некорректный номер поля. Попробуйте еще раз.");
                                numEdit = ChooseNumber();
                            }
                            if (numEdit == 0)
                            {
                                break;
                            }
                            switch (numEdit)
                            {
                                case 1:
                                    ReadSurname(contact);
                                    break;
                                case 2:
                                    ReadName(contact);
                                    break;
                                case 3:
                                    ReadPathronymic(contact);
                                    break;
                                case 4:
                                    ReadPhone(contact);
                                    break;
                                case 5:
                                    ReadCountry(contact);
                                    break;
                                case 6:
                                    ReadDob(contact);
                                    break;
                                case 7:
                                    ReadOrganization(contact);
                                    break;
                                case 8:
                                    ReadPost(contact);
                                    break;
                                case 9:
                                    ReadOther(contact);
                                    break;
                            }

                            phoneBook.EditContact(contact, number);
                            Console.WriteLine("Контакт изменен!");
                            Console.WriteLine("Для выхода в главное меню нажмите любую клавишу.");
                            Console.ReadKey();
                            break;
                        }
                    case '3':
                        {
                            Console.Clear();
                            Console.WriteLine("Удаление контакта\n");
                            int number = phoneBook.ChooseContact();
                            if (number == -1)
                            {
                                break;
                            }
                            phoneBook.DeleteContact(number);
                            Console.WriteLine("Контакт удален!");
                            Console.WriteLine("Для выхода в главное меню нажмите любую клавишу.");
                            Console.ReadKey();
                            break;
                        }
                    case '4':
                        {
                            Console.Clear();
                            Console.WriteLine("Контакты\n");
                            Console.WriteLine("Нажмите любую клавишу, что бы выйти в главное меню\n");
                            phoneBook.ViewContacts();
                            Console.ReadKey();
                            break;
                        }
                    case '5':
                        {
                            Console.Clear();
                            Console.WriteLine("Контакты\n");
                            Console.WriteLine("Нажмите любую клавишу, что бы выйти в главное меню\n");
                            phoneBook.ShortViewContacts();
                            Console.ReadKey();
                            break;
                        }
                }
                Console.Clear();
            }


        }


        public static void ReadSurname(Contact contact)
        {
            bool flag = false;
            Console.WriteLine("Введите фамилию (обязательное поле):");
            while (!flag)
            {
                try
                {
                    contact.Surname = Console.ReadLine();
                    flag = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Попробуйте еще раз:");
                }
            }
        }
        public static void ReadName(Contact contact)
        {
            bool flag = false;
            Console.WriteLine("Введите имя (обязательное поле):");
            while (!flag)
            {
                try
                {
                    contact.Name = Console.ReadLine();
                    flag = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Попробуйте еще раз:");
                }
            }
        }
        public static void ReadCountry(Contact contact)
        {
            bool flag = false;
            Console.WriteLine("Введите страну (обязательное поле):");
            while (!flag)
            {
                try
                {
                    contact.Country = Console.ReadLine();
                    flag = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Попробуйте еще раз:");
                }
            }
        }
        public static void ReadPhone(Contact contact)
        {
            bool flag = false;
            Console.WriteLine("Введите номер телефона (обязательное поле):");
            while (!flag)
            {
                try
                {
                    contact.Phone = Console.ReadLine();
                    flag = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Попробуйте еще раз:");
                }
            }
        }
        public static void ReadPathronymic(Contact contact)
        {
            Console.WriteLine("Введите отчество (что бы пропустить - нажмите Enter):");
            contact.Pathronymic = Console.ReadLine();
        }
        public static void ReadDob(Contact contact)
        {
            Console.WriteLine("Введите дату рождения в формате дд.мм.гггг (что бы пропустить - нажмите Enter):");
            string text = Console.ReadLine();
            if (text == "")
            {
                return;
            }
            DateTime dob;
            bool flag = DateTime.TryParseExact(text, "dd.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dob);
            while (!flag)
            {
                Console.WriteLine("Введена некорректная дата, попробуйте еще раз (что бы пропустить - нажмите Enter):");
                text = Console.ReadLine();
                if (text == "")
                {
                    return;
                }
                flag = DateTime.TryParseExact(text, "dd.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dob);
            }
            contact.Dob = dob;
        }
        public static void ReadOrganization(Contact contact)
        {
            Console.WriteLine("Введите название организации (что бы пропустить - нажмите Enter):");
            contact.Organization = Console.ReadLine();
        }
        public static void ReadPost(Contact contact)
        {
            Console.WriteLine("Введите должность (что бы пропустить - нажмите Enter):");
            contact.Post = Console.ReadLine();
        }
        public static void ReadOther(Contact contact)
        {
            Console.WriteLine("Введите заметки (что бы пропустить - нажмите Enter):");
            contact.Other = Console.ReadLine();
        }

        private static int ChooseNumber()
        {
            string textChoise = Console.ReadLine();
            int numChoise;
            while (!int.TryParse(textChoise, out numChoise) || numChoise < 0)
            {
                Console.WriteLine("Введен некорректный номер, попробуйте еще раз:");
                textChoise = Console.ReadLine();
            }
            return numChoise;
        }










    }
}
